include(ExternalProject)

set(BUILD_GTEST ON)
set(BUILD_GMOCK OFF)
set(INSTALL_GTEST OFF)
set(INSTALL_GMOCK OFF)
ExternalProject_Add(googletest
    URL ${CMAKE_CURRENT_SOURCE_DIR}/googletest-release-1.8.0.zip
    CMAKE_ARGS -DBUILD_GMOCK=OFF -DBUILD_GTEST=ON -DINSTALL_GMOCK=OFF -DINSTALL_GTEST=OFF -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}
)

ExternalProject_Get_Property(googletest STAMP_DIR)
message("googletest stamp dir ${STAMP_DIR}")
include(${STAMP_DIR}/extract-googletest.cmake)

ExternalProject_Get_Property(googletest SOURCE_DIR)
message("googletest source dir ${SOURCE_DIR}")
add_subdirectory(${SOURCE_DIR})

find_path(gtest_INCLUDE_DIR
  gtest/gtest.h
PATHS
  ${SOURCE_DIR}/googletest/include)

# Specify include dir
# ExternalProject_Get_Property(googletest source_dir)
# set(gtest_INCLUDE_DIR ${source_dir}/googletest/include)

# Specify MainTest's link libraries
# ExternalProject_Get_Property(googletest binary_dir)
# set(gtest_SOURCE_DIR ${binary_dir}/googlemock/gtest)

message("gtest source at ${gtest_SOURCE_DIR}")
message("gtest headers at ${gtest_INCLUDE_DIR}")
