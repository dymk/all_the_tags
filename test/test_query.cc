#include <glog/logging.h>
#include <gtest/gtest.h>

#include "context.h"

using namespace att;

class QueryTest : public ::testing::Test {
 public:
  Context ctx;
  Tag *a, *b, *c, *d, *e;

  void SetUp() {
    ctx.mutate([&] {
      CHECK(a = ctx.new_tag());
      CHECK(b = ctx.new_tag());
      CHECK(c = ctx.new_tag());
      CHECK(d = ctx.new_tag());
      CHECK(e = ctx.new_tag());
    });
  }
};

#define SET(expr) std::unordered_set<TagPtr>(expr)

TEST_F(QueryTest, BasicImplication) {
  ctx.mutate([&] { a->imply(b); });

  ctx.mutate([&] {
    c->imply(d);
    d->imply(c);
  });
}

template <typename T>
void same_set(std::unordered_set<T> const& a, std::unordered_set<T> const& b) {
  if (a != b) {
    LOG(INFO) << "a: ";
    for (auto& node : a) {
      LOG(INFO) << node << ", ";
    }
    LOG(INFO) << "b: ";
    for (auto& node : b) {
      LOG(INFO) << node << ", ";
    }
    CHECK(false);
  }
}

TEST_F(QueryTest, ImpliedByTest) {
  ctx.mutate([&] {
    a->imply(b);
    b->imply(c);

    d->imply(e);
    e->imply(d);
  });

  same_set(c->implied_by(), {b});

  same_set(c->deep_impliers(), {a, b, c});

  same_set(d->implied_by(), {e});

  same_set(d->deep_impliers(), {d, e});
}
