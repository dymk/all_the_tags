#include "StringSim.h"
#include "concurrent_service_handler.h"

#include <unordered_set>

namespace string_sim {

class StringSimHandler : virtual public StringSimIf,
                         virtual public common_if::ConcurrentServiceHandler {
 public:
  static const size_t MaxStrLen = 100;

  StringSimHandler() {}

  void reset_context() override;

  void add_tags_locked(
      std::vector<AddTagError>& _return,
      const std::vector<std::string>& tag_values,
      const int32_t lock_generation) override;

  void add_tags(
      std::vector<AddTagError>& _return,
      const std::vector<std::string>& tag_values) override;

  void destroy_tags(
      std::vector<DestroyTagError>& _return,
      const std::vector<std::string>& tag_values) override;

  bool ping() override;

  void match_tags(
      std::vector<StringMatch>& _return,
      const std::string& with_value,
      const int32_t max) override;

 private:
  folly::Synchronized<std::unordered_set<std::string>> state_;
};

} // namespace string_sim
