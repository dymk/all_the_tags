namespace cpp string_sim
namespace rb StringSim

include "concurrent_service_if.thrift"

struct AddTagError {
  1: string tag_value,
  2: string message,
}
struct DestroyTagError {
  1: string tag_value,
  2: string message,
}

struct StringMatch {
  1: string tag_value;
  2: i32 score;
}

service StringSim extends concurrent_service_if.ConcurrentService {
  void reset_context();

  list<AddTagError> add_tags_locked(1: list<string> tag_values, 2: i32 lock_generation)
    throws (1: concurrent_service_if.ExclusiveAccessException ex);

  list<AddTagError> add_tags(1: list<string> tag_values);
  list<DestroyTagError> destroy_tags(1: list<string> tag_values);

  bool ping();

  list<StringMatch> match_tags(1: string with_value, 2: i32 max);
}
