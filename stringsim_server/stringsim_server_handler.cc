#include "stringsim_server_handler.h"

#define FTS_FUZZY_MATCH_IMPLEMENTATION
#include "fts_fuzzy_match.h"

#include <algorithm>
#include <queue>

namespace string_sim {

void StringSimHandler::reset_context() {
  *state_.wlock() = std::unordered_set<std::string>();
}

void StringSimHandler::add_tags_locked(
    std::vector<AddTagError>& _return,
    const std::vector<std::string>& tag_values,
    const int32_t lock_generation) {
  with_generation(
      lock_generation, [&] { this->add_tags(_return, tag_values); });
}

bool StringSimHandler::ping() {
  return true;
}

namespace {
struct Match {
  const std::string* value;
  int score;

  bool operator<(Match const& other) const {
    return score < other.score;
  }
  bool operator>(Match const& other) const {
    return score > other.score;
  }
};
} // namespace

void StringSimHandler::match_tags(
    std::vector<StringMatch>& _return,
    const std::string& with_value,
    const int32_t max_) {
  int32_t max = max_;
  auto state = state_.rlock();

  std::priority_queue<Match, std::vector<Match>, std::greater<Match>> matches;
  // std::priority_queue<Match> matches;

  for (auto const& str : *state) {
    int score;
    if (!fts::fuzzy_match(with_value.c_str(), str.c_str(), score)) {
      continue;
    }

    Match match{&str, score};

    if (matches.size() < max) {
      matches.push(match);
    } else {
      if (match > matches.top()) {
        matches.pop();
        matches.push(match);
      }
    }
  }

  while (matches.size()) {
    auto match = matches.top();
    matches.pop();

    StringMatch m;
    m.tag_value = *match.value;
    m.score = match.score;
    _return.emplace_back(m);
  }

  std::reverse(_return.begin(), _return.end());
}

void StringSimHandler::add_tags(
    std::vector<AddTagError>& _return,
    const std::vector<std::string>& tag_values) {
  auto state = state_.wlock();
  for (auto tag : tag_values) {
    if (tag.size() > MaxStrLen) {
      AddTagError err;
      err.tag_value = tag;
      err.message = "Tag too long";
      _return.emplace_back(std::move(err));
    } else {
      state->insert(tag);
    }
  }
}

void StringSimHandler::destroy_tags(
    std::vector<DestroyTagError>& _return,
    const std::vector<std::string>& tag_values) {
  auto state = state_.wlock();
  for (auto tag : tag_values) {
    state->erase(tag);
  }
}

} // namespace string_sim
