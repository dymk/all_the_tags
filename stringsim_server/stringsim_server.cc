#include "common_server.h"
#include "stringsim_server_handler.h"

int main(int argc, char** argv) {
  common_init(argc, argv);

  auto handler = std::make_shared<string_sim::StringSimHandler>();
  auto processor = std::make_shared<string_sim::StringSimProcessor>(handler);

  start_handler(processor, "StringSim");
  return 0;
}
