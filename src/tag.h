#pragma once

#include <memory>
#include <unordered_set>

#include "id.h"
#include "ptrs.h"

namespace att {

struct Tag;
using TagPtr = Tag*;

// needs forward declaration because C++ uses goddamn textual inclusion
struct Context;

struct Tag : std::enable_shared_from_this<Tag> {
  enum class PinState { No, InProgress, Yes };

 private:
  id_type id_;

  // TODO: implement tag implications
  std::unordered_set<TagPtr> implies_;
  std::unordered_set<TagPtr> implied_by_;

#ifdef DEBUG
  Context& context_;
#endif
  PinState pinned_;

 public:
  Tag(Context& context, id_type id)
      : id_(id),
        pinned_(PinState::No)
#ifdef DEBUG
        ,
        context_(context)
#endif
  {
  }

  // this tag implies -> other tag
  bool imply(TagPtr other);
  bool unimply(TagPtr other);

  // entirely remove from the implication graph
  void remove();

  std::unordered_set<TagPtr> const& implies() const {
    return implies_;
  }

  std::unordered_set<TagPtr> const& implied_by() const {
    return implied_by_;
  }

  // all tags which imply this tag
  std::unordered_set<TagPtr> deep_impliers();
  std::unordered_set<TagPtr> deep_implies();

  // Deep impliers, but stop the search at pinned tags
  std::unordered_set<TagPtr> deep_impliers_pinned_stops();

  id_type id() const {
    return id_;
  }

  PinState pinned() const {
    return pinned_;
  }
  void mark_pinned(PinState p) {
    pinned_ = p;
  }
};

} // namespace att

// custom specialization of std::hash can be injected in namespace std
namespace std {
template <>
struct hash<att::TagPtr const> {
  typedef att::TagPtr const argument_type;
  typedef std::size_t result_type;
  result_type operator()(argument_type const& s) const noexcept {
    return std::hash<att::TagPtr>{}(const_cast<att::TagPtr>(s));
  }
};
} // namespace std
