#pragma once

namespace att {

// Represents a (possibly persistant) backing store for an att::Context
class Store {
  virtual void tag_created(Tag const&) = 0;
  virtual void tag_destroyed(Tag const&) = 0;
  virtual void ent_created(Entity const&) = 0;
  virtual void ent_destroyed(Entity const&) = 0;

  virtual Context::id_to_tag_map load_tags(Context& c);
  virtual Context::id_to_tag_map load_ents(Context& c);
};

class MemoryStore : public Store {};

} /* namespace att */
