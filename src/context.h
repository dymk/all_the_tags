#pragma once

#include <algorithm>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "id.h"
#include "ptrs.h"

#include <folly/Optional.h>
#include <folly/ScopeGuard.h>

namespace att {

struct Context {
 private:
  id_type last_tag_id{0};

  // TODO: make this a map from id_type to unique pointer of tag
  // because we can likely have this map be the proper owner
  // of the tag memory. Destroying a tag should remove all references
  // to it. Maybe verify this in debug mode with a custom refcounting
  // scheme?
  std::unordered_map<id_type, std::unique_ptr<Tag>> id_to_tag{};

  bool can_mutate_{false};

  // internals
  Tag* new_tag_common(id_type id);

 public:
  Context() {}
  ~Context(){};

  template <typename Func>
  void mutate(Func&& func) {
    can_mutate_ = true;
    auto g = folly::makeGuard([&]() { can_mutate_ = false; });
    func();
  }

  bool can_mutate() const {
    return can_mutate_;
  }

  // returns a new tag (or null)
  Tag* new_tag();
  Tag* new_tag(id_type id);

  // inverse of new_tag
  bool destroy_tag(id_type);

  // context statistics
  size_t num_tags() const {
    return id_to_tag.size();
  }

  Tag* tag_by_id(id_type) const;
};

} /* namespace att */
