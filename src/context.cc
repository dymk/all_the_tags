#include <functional>
#include <queue>
#include <stack>

#include <glog/logging.h>

#include "context.h"
#include "tag.h"

namespace att {

namespace {
static constexpr bool debug = false;
}

Tag* Context::new_tag_common(id_type id) {
  auto t = std::make_unique<Tag>(*this, id);
  auto ret = t.get();
  this->id_to_tag.insert(std::make_pair(id, std::move(t)));
  return ret;
}

Tag* Context::new_tag(id_type id) {
  CHECK(can_mutate());

  if (id_to_tag.find(id) != id_to_tag.end()) {
    return nullptr;
  }

  return new_tag_common(id);
}

Tag* Context::new_tag() {
  CHECK(can_mutate());

  while (true) {
    id_type id = last_tag_id++;
    if (id_to_tag.find(id) == id_to_tag.end()) {
      return new_tag_common(id);
    }
  }
}

bool Context::destroy_tag(id_type tag_id) {
  CHECK(can_mutate());

  auto tag_iter = id_to_tag.find(tag_id);
  if (tag_iter == id_to_tag.end()) {
    return false;
  }
  auto& tag = tag_iter->second;
  tag->remove();

  CHECK_EQ(1, id_to_tag.erase(tag->id()));

  return true;
}

Tag* Context::tag_by_id(id_type id) const {
  auto iter = id_to_tag.find(id);
  if (iter == id_to_tag.end()) {
    return nullptr;
  }

  return iter->second.get();
}

} /* namespace att */
