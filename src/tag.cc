#include "tag.h"
#include "context.h"

#include <glog/logging.h>

#include <stack>
#include <unordered_set>

namespace att {

void Tag::remove() {
#ifdef DEBUG
  CHECK(context_.can_mutate());
#endif

  // remove all impliers
  while (!this->implies_.empty()) {
    this->unimply(*(this->implies_.begin()));
  }

  // remove all implied by
  while (!this->implied_by_.empty()) {
    TagPtr implier = *(this->implied_by_.begin());
    implier->unimply(this);
  }
}

bool Tag::imply(TagPtr other) {
#ifdef DEBUG
  CHECK(context_.can_mutate());
#endif

  if (this == other) {
    return false;
  }

  auto a = other->implied_by_.insert(this).second;
  auto b = implies_.insert(other).second;
  CHECK_EQ(a, b);

  return a;
}
bool Tag::unimply(TagPtr other) {
#ifdef DEBUG
  CHECK(context_.can_mutate());
#endif

  if (this == other) {
    return false;
  }

  auto a = other->implied_by_.erase(this) == 1;
  auto b = implies_.erase(other) == 1;
  CHECK_EQ(a, b);

  return a;
}

template <typename TagToList>
static std::unordered_set<TagPtr> deep_traverse_relation(
    TagPtr tag,
    TagToList&& ttl) {
  std::unordered_set<TagPtr> ret;

  std::stack<TagPtr> to_visit;
  to_visit.push(tag);

  while (!to_visit.empty()) {
    auto visiting = to_visit.top();
    to_visit.pop();
    ret.insert(visiting);

    ttl(visiting, [&](auto node) {
      if (ret.find(node) == ret.end()) {
        to_visit.push(node);
      }
    });
  }

  return std::move(ret);
}

std::unordered_set<TagPtr> Tag::deep_impliers() {
  return deep_traverse_relation(this, [](auto tag, auto const& cb) {
    for (auto node : tag->implied_by_) {
      cb(node);
    }
  });
}
std::unordered_set<TagPtr> Tag::deep_implies() {
  return deep_traverse_relation(this, [](auto tag, auto const& cb) {
    for (auto node : tag->implies_) {
      cb(node);
    }
  });
}

std::unordered_set<TagPtr> Tag::deep_impliers_pinned_stops() {
  VLOG(4) << "getting deep_impliers_pinned_stops for " << id();
  return deep_traverse_relation(this, [](auto tag, auto const& cb) {
    if (tag->pinned() == PinState::Yes) {
      VLOG(4) << "implier tag " << tag->id() << " is pinned; stopping here";
      return;
    }

    for (auto node : tag->implied_by_) {
      cb(node);
    }
  });
}

} // namespace att
