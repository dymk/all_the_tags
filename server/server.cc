#include "common_server.h"
#include "server_handler.h"

int main(int argc, char** argv) {
  common_init(argc, argv);

  std::shared_ptr<AttServerHandler> handler(new AttServerHandler());
  std::shared_ptr<apache::thrift::TProcessor> processor(
      new att::server::AttServerProcessor(handler));

  start_handler(processor, "AttServer");
  return 0;
}
