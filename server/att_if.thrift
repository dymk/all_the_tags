namespace cpp att.server
namespace rb att.server

include "concurrent_service_if.thrift"

typedef i32 id_type;

struct TagError {
  1: id_type id,
  2: string message,
}

struct TagList {
  1: list<id_type> ids
}

struct ImplyError {
  1: id_type implier,
  2: id_type implied,
  3: string message,
}

exception QueryError {
  1: string query,
  2: string message,
}

enum PinState {
  No, InProgress, Yes
}
union IdListOrTagError {
  1: list<id_type> tags;
  2: TagError error;
}

struct ConfigureTag {
  1: id_type id;

  2: bool change_pinned;
  3: PinState pinned;
}

service AttServer extends concurrent_service_if.ConcurrentService {
  void reset_context();

  // batch tag creation, in a locked context
  list<TagError> create_tag_locked(1: list<id_type> tag_ids, 2: i32 lock_generation)
    throws (1: concurrent_service_if.ExclusiveAccessException ex);
  list<ImplyError> create_implication_locked(1: map<id_type, list<id_type>> imply_list, 2: i32 lock_generation)
    throws (1: concurrent_service_if.ExclusiveAccessException ex);
  list<TagError> configure_tag_locked(1: list<ConfigureTag> configure_tags, 2: i32 lock_generation)
    throws (1: concurrent_service_if.ExclusiveAccessException ex);

  // for implementing heartbeat
  bool ping();

  // batch tag creation
  list<TagError> create_tag(1: list<id_type> tag_ids);
  list<TagError> configure_tag(1: list<ConfigureTag> configure_tags);
  list<TagError> destroy_tag(1: list<id_type> tag_ids);

  // batch tag implication
  list<ImplyError> create_implication(1: map<id_type, list<id_type>> imply_list);
  list<ImplyError> destroy_implication(1: map<id_type, list<id_type>> imply_list);

  // who implies this tag?
  IdListOrTagError single_deep_implies(1: id_type id);
  IdListOrTagError single_deep_impliers(1: id_type id);

  string generate_query(1: string query)
    throws (1: QueryError e);

  // calculate minimal list of tags, removing duplicates
  IdListOrTagError compute_tags_from_list(1: list<id_type> tag_list);
}
