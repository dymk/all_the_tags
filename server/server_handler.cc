#include "server_handler.h"

void AttServerHandler::reset_context() {
  LOG(INFO) << "Resetting context...";
  auto c = context_sync_.wlock();
  c->context.~Context();
  new (&c->context) att::Context();
}

void AttServerHandler::generate_query(std::string& ret, std::string const& q) {
  LOG(INFO) << "Got query: " << q;
  auto c = context_sync_.rlock();

  folly::StringPiece cursor(q);
  while (!cursor.empty()) {
    // ensure forward progress
    auto fg = folly::makeGuard(
        [&cursor, s = cursor.size()] { CHECK(s > cursor.size()) << cursor; });

    if (cursor.front() == '&' || cursor.front() == '|' ||
        cursor.front() == '!' || cursor.front() == '(' ||
        cursor.front() == ')') {
      VLOG(3) << "query: got char: " << cursor.front();
      ret += cursor.front();
      cursor.advance(1);
    } else if (cursor.front() == '^') {
      cursor.advance(1);
      ret += "(-1)";
    } else if (std::isdigit(cursor.front())) {
      // alters cursor to move past id
      att::id_type id = folly::to<att::id_type>(&cursor);
      VLOG(3) << "query: parsed lit: " << id;

      auto tag = c->context.tag_by_id(id);
      if (!tag) {
        att::server::QueryError err;
        err.query = q;
        folly::toAppend("no tag with id ", id, " found", &err.message);
        throw err;
      }

      std::vector<int32_t> all_impliers;

      auto imps_set = tag->deep_impliers_pinned_stops();
      all_impliers.reserve(imps_set.size());
      CHECK(imps_set.size() >= 1);
      for (auto t : imps_set) {
        all_impliers.push_back(t->id());
      }

      std::sort(all_impliers.begin(), all_impliers.end());

      folly::toAppend("(", &ret);
      auto implier = all_impliers.begin();
      for (int i = 0; i < all_impliers.size(); i++) {
        // auto const id = (*implier)->id();
        auto const id = *implier;
        VLOG(3) << "query: above tag (pinned: "
                << static_cast<int>(tag->pinned()) << ") implied by: " << id;
        folly::toAppend(id, &ret);

        if (i < all_impliers.size() - 1) {
          folly::toAppend("|", &ret);
        }

        implier++;
      }
      folly::toAppend(")", &ret);

    } else {
      fg.dismiss();
      att::server::QueryError err;
      err.query = q;
      folly::toAppend("unknown char '", cursor.front(), "'", &err.message);
      VLOG(3) << "error parsing query: " << err.message << " (query: " << q
              << ")";
      throw err;
    }
  }

  if (ret.size()) {
    if (ret.back() == ' ') {
      ret = ret.substr(0, ret.size() - 1);
    }
  }

  VLOG(3) << "transformed query: ";
  VLOG(3) << " '" << q << "' ->";
  VLOG(3) << " '" << ret << "'";
}

template <typename RunOnTags>
void AttServerHandler::mutate_implication(
    std::vector<att::server::ImplyError>& ret,
    const std::map<int32_t, std::vector<int32_t>>& imply_list,
    RunOnTags const& callback) {
  auto c = context_sync_.wlock();
  c->context.mutate([&] {
    for (auto pair : imply_list) {
      auto implier = c->context.tag_by_id(pair.first);
      if (!implier) {
        att::server::ImplyError err;
        err.implier = pair.first;
        err.implied = -1;
        err.message = "implier id not found";
        ret.push_back(err);
        continue;
      }
      for (auto implied_id : pair.second) {
        auto implied = c->context.tag_by_id(implied_id);

        if (!implied) {
          att::server::ImplyError err;
          err.implier = pair.first;
          err.implied = implied_id;
          err.message = "implied id not found";
          ret.push_back(err);
          continue;
        }

        VLOG(2) << "implying " << pair.first << " -> " << implied_id;
        if (!callback(implier, implied)) {
          att::server::ImplyError err;
          err.implied = pair.first;
          err.implied = implied_id;
          err.message = "error implying";
          ret.push_back(err);
        }
      }
    }
  });
}

void AttServerHandler::create_implication(
    std::vector<att::server::ImplyError>& ret,
    const std::map<int32_t, std::vector<int32_t>>& imply_list) {
  mutate_implication(ret, imply_list, [](auto implier, auto implied) {
    return implier->imply(implied);
  });
}

void AttServerHandler::destroy_implication(
    std::vector<att::server::ImplyError>& ret,
    const std::map<int32_t, std::vector<int32_t>>& imply_list) {
  mutate_implication(ret, imply_list, [](auto implier, auto implied) {
    return implier->unimply(implied);
  });
}

void AttServerHandler::create_tag_locked(
    std::vector<att::server::TagError>& ret,
    std::vector<int32_t> const& tag_ids,
    const int32_t lock_generation) {
  with_generation(lock_generation, [&] { create_tag(ret, tag_ids); });
}

void AttServerHandler::create_implication_locked(
    std::vector<att::server::ImplyError>& ret,
    const std::map<int32_t, std::vector<int32_t>>& imply_list,
    const int32_t lock_generation) {
  with_generation(
      lock_generation, [&] { create_implication(ret, imply_list); });
}

void AttServerHandler::configure_tag_locked(
    std::vector<att::server::TagError>& ret,
    std::vector<att::server::ConfigureTag> const& configure_tags,
    const int32_t lock_generation) {
  with_generation(lock_generation, [&] { configure_tag(ret, configure_tags); });
}

static folly::Optional<att::Tag::PinState> thrift_enum_to_pin_state(
    att::server::PinState::type s) {
  switch (s) {
    case att::server::PinState::No:
      return {att::Tag::PinState::No};
    case att::server::PinState::InProgress:
      return {att::Tag::PinState::InProgress};
    case att::server::PinState::Yes:
      return {att::Tag::PinState::Yes};
    default:
      return {};
  }
}

void AttServerHandler::configure_tag(
    std::vector<att::server::TagError>& ret,
    std::vector<att::server::ConfigureTag> const& configure_tags) {
  auto c = context_sync_.rlock();
  for (auto const& ct : configure_tags) {
    auto tag = c->context.tag_by_id(ct.id);
    if (!tag) {
      att::server::TagError err;
      err.id = ct.id;
      err.message = "tag not found";
      ret.push_back(err);
      continue;
    }

    if (ct.change_pinned) {
      auto ps = thrift_enum_to_pin_state(ct.pinned);
      if (ps) {
        VLOG(3) << "changing pinning of " << tag->id() << " from "
                << static_cast<int>(tag->pinned()) << " to "
                << static_cast<int>(*ps);
        tag->mark_pinned(*ps);
      } else {
        att::server::TagError err;
        err.id = ct.id;
        err.message = "invalid pin state";
        ret.push_back(err);
        continue;
      }
    }
  }
}

template <typename LockPtr, typename TagToRelation>
auto single_deep_relation_tags(
    att::server::IdListOrTagError& ret,
    int32_t const id,
    LockPtr& c,
    TagToRelation&& ttr) {
  auto tag = c->context.tag_by_id(id);
  if (!tag) {
    ret.__isset.error = true;
    ret.error.id = id;
    ret.error.message = "id not found";
    return;
  }

  auto deep_tags = ttr(tag);
  ret.__isset.tags = true;
  ret.tags.reserve(deep_tags.size());
  for (auto tag : deep_tags) {
    ret.tags.push_back(tag->id());
  }
  std::sort(ret.tags.begin(), ret.tags.end());
}

void AttServerHandler::single_deep_implies(
    att::server::IdListOrTagError& ret,
    const int32_t id) {
  auto c = context_sync_.rlock();

  single_deep_relation_tags(
      ret, id, c, [](auto tag) { return tag->deep_implies(); });
}

void AttServerHandler::single_deep_impliers(
    att::server::IdListOrTagError& ret,
    const int32_t id) {
  auto c = context_sync_.rlock();

  single_deep_relation_tags(
      ret, id, c, [](auto tag) { return tag->deep_impliers(); });
}

void AttServerHandler::compute_tags_from_list(
    att::server::IdListOrTagError& ret,
    std::vector<int32_t> const& tag_id_list) {
  auto c = context_sync_.rlock();

  // cache of tag -> implied tags
  std::unordered_map<att::TagPtr, std::unordered_set<att::TagPtr>>
      trans_closure_cache;

  std::list<att::TagPtr> tag_list;

  if (VLOG_IS_ON(2)) {
    std::stringstream ss;
    for (auto t : tag_id_list) {
      ss << t << ", ";
    }
    VLOG(2) << "computing tag list from list " << ss.str();
  }

  for (auto t : tag_id_list) {
    auto tag = c->context.tag_by_id(t);
    if (!tag) {
      ret.__isset.error = true;
      ret.error.message = "tag not found";
      ret.error.id = t;
      return;
    }

    tag_list.push_back(tag);
    trans_closure_cache[tag] = tag->deep_implies();
  }

  for (auto tag : tag_list) {
    VLOG(2) << "considering tag " << tag->id();

    auto& implies = trans_closure_cache[tag];
    tag_list.remove_if([&](auto t) {
      auto ret = (t != tag) && (implies.find(t) != implies.end());
      VLOG_IF(2, ret) << " -> " << t->id() << " is implied, removing";
      return ret;
    });
  }

  // find all pinned tags (even if they're not explicitly listed by the user) in
  // the transitive closure and add them to the computed tags list
  for (auto tag : tag_list) {
    for (auto implied : trans_closure_cache[tag]) {
      if (implied->pinned() != att::Tag::PinState::No) {
        VLOG(3) << "Pinned tag " << implied->id()
                << " being added to compute list";
        tag_list.push_front(implied);
      }
    }
  }

  // remove any duplicate tags in the list
  tag_list.sort([](auto a, auto b) { return a->id() < b->id(); });
  tag_list.unique();

  ret.__isset.tags = true;
  ret.tags.reserve(tag_list.size());
  for (auto tag : tag_list) {
    ret.tags.push_back(tag->id());
  }
}
