#pragma once

#include "context.h"

#include <gflags/gflags.h>
#include <glog/logging.h>

#include <folly/Conv.h>
#include <folly/Synchronized.h>

#include <chrono>

#include "AttServer.h"
#include "concurrent_service_handler.h"

class AttServerHandler : virtual public att::server::AttServerIf,
                         virtual public common_if::ConcurrentServiceHandler {
 public:
  AttServerHandler() {}

  void reset_context() override;

  bool ping() override {
    return true;
  }

  template <typename RunOnTagId>
  void mutate_tag_list(
      std::vector<att::server::TagError>& ret,
      std::vector<int32_t> const& tag_ids,
      std::string const& error_string,
      RunOnTagId const& callback) {
    auto c = context_sync_.wlock();
    c->context.mutate([&] {
      for (auto id : tag_ids) {
        VLOG(2) << " -> " << id;
        auto t = callback(c->context, id);
        if (!t) {
          VLOG(2) << error_string << " " << id;
          att::server::TagError err;
          err.id = id;
          err.message = error_string;
          ret.push_back(err);
        }
      }
    });
  }

  void create_tag_locked(
      std::vector<att::server::TagError>& ret,
      std::vector<int32_t> const& tag_ids,
      const int32_t lock_generation) override;
  void create_implication_locked(
      std::vector<att::server::ImplyError>& ret,
      const std::map<int32_t, std::vector<int32_t>>& imply_list,
      const int32_t lock_generation) override;
  void configure_tag_locked(
      std::vector<att::server::TagError>& ret,
      std::vector<att::server::ConfigureTag> const& configure_tags,
      const int32_t lock_generation) override;

  void configure_tag(
      std::vector<att::server::TagError>& ret,
      std::vector<att::server::ConfigureTag> const& configure_tags) override;

  void create_tag(
      std::vector<att::server::TagError>& ret,
      std::vector<int32_t> const& tag_ids) override {
    VLOG(2) << "create_tag";
    mutate_tag_list(
        ret, tag_ids, "Error creating tag", [](auto& c, auto tag_id) {
          return c.new_tag(tag_id);
        });
  }

  void destroy_tag(
      std::vector<att::server::TagError>& ret,
      std::vector<int32_t> const& tag_ids) override {
    VLOG(2) << "destroy_tag";
    mutate_tag_list(
        ret, tag_ids, "Error destroying tag", [](auto& c, auto tag_id) {
          return c.destroy_tag(tag_id);
        });
  }

  void generate_query(std::string& ret, std::string const& q) override;

  template <typename RunOnTags>
  void mutate_implication(
      std::vector<att::server::ImplyError>& ret,
      const std::map<int32_t, std::vector<int32_t>>& imply_list,
      RunOnTags const& callback);

  void create_implication(
      std::vector<att::server::ImplyError>& ret,
      const std::map<int32_t, std::vector<int32_t>>& imply_list) override;
  void destroy_implication(
      std::vector<att::server::ImplyError>& ret,
      const std::map<int32_t, std::vector<int32_t>>& imply_list) override;

  void single_deep_implies(att::server::IdListOrTagError& ret, const int32_t id)
      override;
  void single_deep_impliers(
      att::server::IdListOrTagError& ret,
      const int32_t id) override;

  void compute_tags_from_list(
      att::server::IdListOrTagError& ret,
      std::vector<int32_t> const&) override;

 private:
  struct ContextSynchronized {
    att::Context context;
  };
  folly::Synchronized<ContextSynchronized> context_sync_;
};
