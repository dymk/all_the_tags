#include "AttServer.h"

#include <protocol/TBinaryProtocol.h>
#include <protocol/TCompactProtocol.h>
#include <transport/TBufferTransports.h>
#include <transport/TSocket.h>

#include <gflags/gflags.h>
#include <glog/logging.h>

#include <thread>

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

DEFINE_string(host, "localhost", "Host of the ATT server");
DEFINE_int32(port, 9090, "Port of the ATT server");
DEFINE_string(protocol, "binary", "Thrift protocol to use");

void simulate_locked_client(att::server::AttServerClient& client) {
  while (true) {
    common_if::LockedResult lr;
    client.lock(lr, 2); // 2 second timeout

    if (lr.got_lock) {
      // we got the lock!
      LOG(INFO) << "Client got lock (gen " << lr.lock_generation
                << "), initializing...";

      if (lr.was_initialized) {
        LOG(INFO) << "Already initialized!";
        client.unlock(lr.lock_generation, true);
        return;
      }

      LOG(INFO) << "Wasn't already initialized!";
      client.reset_context();

      for (int i = 0; i < 10; i++) {
        std::vector<att::server::TagError> errs;
        LOG(INFO) << "Create tag << " << i
                  << ", artificial delay, reset watchdog";
        client.create_tag_locked(errs, {i}, lr.lock_generation);
        CHECK_EQ(errs.size(), 0);
        std::this_thread::sleep_for(std::chrono::milliseconds{500});
        client.kick_lock_watchdog(lr.lock_generation);
      }

      LOG(INFO) << "Unlocking gen " << lr.lock_generation;
      client.unlock(lr.lock_generation, true); // mark initialized
      break;
    } else {
      while (client.poll_is_locked()) {
        LOG(INFO) << "Client didn't get lock, waiting...";
        std::this_thread::sleep_for(std::chrono::seconds{1});
      }
    }
  }
}

template <typename T>
decltype(auto) operator<<(std::ostream& o, std::vector<T> const& vec) {
  for (auto& t : vec) {
    o << t << ", ";
  }
  return o;
}

template <typename Client>
auto create_tags(Client& client, std::vector<int32_t> const& ids) {
  LOG(INFO) << "create_tag(num: " << ids << ")";
  std::vector<att::server::TagError> errs;
  client.create_tag(errs, ids);
  for (auto& err : errs) {
    LOG(ERROR) << "err: " << err.id << ": " << err.message;
  }
  return std::move(errs);
}

int main(int argc, char** argv) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;

  std::shared_ptr<TSocket> socket(new TSocket(FLAGS_host, FLAGS_port));
  std::shared_ptr<TTransport> transport(new TBufferedTransport(socket));

  std::string prot = FLAGS_protocol;
  std::shared_ptr<TProtocol> protocol;
  if (prot == "binary") {
    protocol = std::make_shared<TBinaryProtocol>(transport);
  } else if (prot == "compact") {
    protocol = std::make_shared<TCompactProtocol>(transport);
  } else {
    LOG(FATAL) << "invalid protocol " << prot;
  }

  att::server::AttServerClient client(protocol);
  transport->open();

  // simulate locking the server
  // simulate_locked_client(client);

  // return 0;

  client.reset_context();

  CHECK_EQ(0, create_tags(client, {1, 2, 3, 4, 5, 6}).size());

  {
    LOG(INFO) << "create_implication(1 -> 2, 2 -> 3)";
    std::vector<att::server::ImplyError> err;
    client.create_implication(
        err,
        {
            {1, {2}},
            {2, {3}},
            {5, {6}},
            {6, {5}},
        });
    CHECK(err.empty());
  }

  {
    LOG(INFO) << "single_deep_impliers(3): ";
    att::server::IdListOrTagError ret;
    client.single_deep_impliers(ret, 3);
    CHECK(ret.__isset.tags);
    for (auto id : ret.tags) {
      LOG(INFO) << "-> " << id;
    }
  }

  auto gen_query = [&](std::string const& str) {
    LOG(INFO) << "generate_query(\"" << str << "\")";
    std::string ret;
    client.generate_query(ret, str);
    LOG(INFO) << "-> \"" << ret << '"';
    return ret;
  };

  CHECK_EQ(gen_query("1"), "(1)");
  CHECK_EQ(gen_query("2|4"), "(1|2)|(4)");
  CHECK_EQ(gen_query("3|4"), "(1|2|3)|(4)");
  CHECK_EQ(gen_query("6"), "(5|6)");

  CHECK_EQ(0, create_tags(client, {7, 8, 9, 10, 11}).size());
  {
    LOG(INFO) << "create_implication(7 -> 8 -> 9 -> 10 -> 11)";
    std::vector<att::server::ImplyError> err;
    client.create_implication(
        err,
        {
            {7, {8}},
            {8, {9}},
            {9, {10}},
            {10, {11}},
        });
    CHECK(err.empty());
  }

  auto check_deep_eq = [&](std::vector<int32_t> const& expect, auto& actual) {
    CHECK(actual.__isset.tags);
    CHECK(!actual.__isset.error);
    CHECK(expect == actual.tags)
        << "expect " << expect << " actual " << actual.tags;
  };

  LOG(INFO) << "check deep implies graph";
  {
    att::server::IdListOrTagError id_list;
    client.single_deep_impliers(id_list, 9);
    check_deep_eq({7, 8, 9}, id_list);

    client.single_deep_implies(id_list, 9);
    check_deep_eq({9, 10, 11}, id_list);

    client.single_deep_implies(id_list, 10);
    check_deep_eq({10, 11}, id_list);
  }

  {
    std::vector<att::server::ImplyError> err;
    client.create_implication(err, {{11, {9}}});
    CHECK_EQ(0, err.size()) << "err: " << err;
  }

  for (int id = 9; id <= 11; id++) {
    LOG(INFO) << "check 9->10->11 (" << id << ")";
    att::server::IdListOrTagError ret;
    client.single_deep_implies(ret, id);
    check_deep_eq({9, 10, 11}, ret);
    client.single_deep_impliers(ret, id);
    check_deep_eq({7, 8, 9, 10, 11}, ret);
  }

  {
    LOG(INFO) << "check minimal set which maintains same transitive closure";
    att::server::IdListOrTagError id_list;
    client.compute_tags_from_list(id_list, {1, 2, 3});
    check_deep_eq({1}, id_list);
    LOG(INFO) << "{1, 2, 3} passes";

    client.compute_tags_from_list(id_list, {2, 3});
    check_deep_eq({2}, id_list);

    client.compute_tags_from_list(id_list, {1});
    check_deep_eq({1}, id_list);
    LOG(INFO) << "{1} passes";

    client.compute_tags_from_list(id_list, {5, 6});
    check_deep_eq({5}, id_list);
    LOG(INFO) << "{5} passes";

    client.compute_tags_from_list(id_list, {6, 5});
    check_deep_eq({6}, id_list);
    LOG(INFO) << "{6} passes";
  }

  auto make_pinstate = [](auto id, auto state) {
    att::server::ConfigureTag ct;
    ct.id = id;
    ct.change_pinned = true;
    ct.pinned = state;
    return ct;
  };

  using PS = att::server::PinState;

  {
    LOG(INFO) << "test tag pinning, pinning tag 3 InProgress";
    att::server::ConfigureTag ct;
    std::vector<att::server::TagError> err;
    client.configure_tag(
        err,
        {
            make_pinstate(3, PS::InProgress),
        });
    CHECK_EQ(0, err.size());

    LOG(INFO) << "testing if computed list has 3 in it";
    att::server::IdListOrTagError id_list;
    client.compute_tags_from_list(id_list, {1, 2, 3});
    check_deep_eq({1, 3}, id_list);
    CHECK_EQ(gen_query("1"), "(1)");
    CHECK_EQ(gen_query("2"), "(1|2)");
    CHECK_EQ(gen_query("3"), "(1|2|3)");

    LOG(INFO) << "test tag pinning, pinning tag 3 Yes";
    client.configure_tag(
        err,
        {
            make_pinstate(3, PS::Yes),
        });
    CHECK_EQ(0, err.size());
    CHECK_EQ(gen_query("1"), "(1)");
    CHECK_EQ(gen_query("2"), "(1|2)");
    CHECK_EQ(gen_query("3"), "(3)");
    client.compute_tags_from_list(id_list, {1});
    check_deep_eq({1, 3}, id_list);
    client.compute_tags_from_list(id_list, {1, 2});
    check_deep_eq({1, 3}, id_list);

    LOG(INFO) << "testing intermediary pinned tags";
    client.configure_tag(
        err,
        {
            make_pinstate(2, PS::Yes),
            make_pinstate(3, PS::No),
        });
    CHECK_EQ(0, err.size());
    CHECK_EQ(gen_query("1"), "(1)");
    CHECK_EQ(gen_query("2"), "(2)");
    CHECK_EQ(gen_query("3"), "(2|3)");
    client.compute_tags_from_list(id_list, {1, 2, 3});
    check_deep_eq({1, 2}, id_list);

    LOG(INFO) << "testing intermediary InProgress tags";
    client.configure_tag(
        err,
        {
            make_pinstate(2, PS::InProgress),
        });
    CHECK_EQ(0, err.size());
    CHECK_EQ(gen_query("1"), "(1)");
    CHECK_EQ(gen_query("2"), "(1|2)");
    CHECK_EQ(gen_query("3"), "(1|2|3)");
  }

  transport->close();

  return 0;
}
