namespace cpp common_if

struct LockedResult {
  1: bool got_lock;
  2: bool was_initialized;
  3: i32 lock_generation;
}

exception ExclusiveAccessException {
  1: string message;
}

service ConcurrentService {
  LockedResult lock(1: i32 timeout_secs);
  // must be called before `timeout_secs` seconds after calling lock(timeout_secs)
  void kick_lock_watchdog(1: i32 lock_generation) throws (1: ExclusiveAccessException ex);
  // release exclusive access to the database, and mark if the DB is initialized or not
  void unlock(1: i32 lock_generation, 2: bool mark_initialized) throws (1: ExclusiveAccessException ex);
  // is the server still locked and the locking connection hasn't timed out?
  bool poll_is_locked();
}
