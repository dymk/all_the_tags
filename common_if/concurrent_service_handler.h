#pragma once

#include "ConcurrentService.h"

#include <folly/Conv.h>
#include <folly/Synchronized.h>

#include <chrono>

namespace common_if {

class ConcurrentServiceHandler : virtual public common_if::ConcurrentServiceIf {
 public:
  void lock(common_if::LockedResult& _return, const int32_t timeout_secs)
      override;
  bool poll_is_locked() override;
  void kick_lock_watchdog(const int32_t lock_generation) override;
  void unlock(const int32_t lock_generation, const bool mark_initialized)
      override;

  static void check_right_lock_gen(int32_t given, int32_t current) {
    if (given != current) {
      common_if::ExclusiveAccessException ex;
      folly::toAppend(
          "Wrong generation kicking watchdog (",
          given,
          " vs ",
          current,
          ")",
          &ex.message);
      throw ex;
    }
  }
  static void check_is_locked(bool locked) {
    if (!locked) {
      common_if::ExclusiveAccessException ex;
      ex.message = "Server isn't locked";
      throw ex;
    }
  }

  template <typename Cb>
  void with_generation(int32_t lock_generation, Cb const& cb) {
    auto r = exclusive_info_.rlock();
    check_is_locked(r->is_locked);
    check_right_lock_gen(r->lock_generation, lock_generation);
    cb();
  }

 private:
  struct SetupSynchronized {
    bool is_locked{false};
    bool was_initialized{false};

    int32_t lock_generation{0};
    std::chrono::system_clock::time_point locked_at;
    std::chrono::seconds timeout_secs{0};

    bool timed_out() const {
      return (locked_at + timeout_secs) < std::chrono::system_clock::now();
    }

    void kick_watchdog() {
      locked_at = std::chrono::system_clock::now();
    }
  };
  folly::Synchronized<SetupSynchronized> exclusive_info_;
};

} // namespace common_if
