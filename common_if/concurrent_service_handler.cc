#include "concurrent_service_handler.h"

namespace common_if {

void ConcurrentServiceHandler::lock(
    common_if::LockedResult& _return,
    const int32_t timeout_secs) {
  auto r = exclusive_info_.wlock();

  _return.got_lock = false;
  if (!r->is_locked || (r->is_locked && r->timed_out())) {
    _return.got_lock = true;
    r->lock_generation += 1;
    r->timeout_secs = std::chrono::seconds{timeout_secs};
    r->kick_watchdog();
  }

  r->is_locked = true;

  _return.was_initialized = r->was_initialized;
  _return.lock_generation = r->lock_generation;
}

bool ConcurrentServiceHandler::poll_is_locked() {
  // Your implementation goes here
  auto r = exclusive_info_.rlock();
  if (r->is_locked && r->timed_out()) {
    return false;
  }

  return r->is_locked;
}

void ConcurrentServiceHandler::kick_lock_watchdog(
    int32_t const lock_generation) {
  auto r = exclusive_info_.wlock();
  check_is_locked(r->is_locked);
  check_right_lock_gen(r->lock_generation, lock_generation);

  r->kick_watchdog();
}

void ConcurrentServiceHandler::unlock(
    const int32_t lock_generation,
    const bool mark_initialized) {
  common_if::ExclusiveAccessException ex;

  auto r = exclusive_info_.wlock();
  check_is_locked(r->is_locked);
  check_right_lock_gen(r->lock_generation, lock_generation);

  r->was_initialized = mark_initialized;
  r->is_locked = false;
}

} /* namespace common_if */
