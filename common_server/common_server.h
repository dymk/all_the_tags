#pragma once

#include <server/TServer.h>

#include <signal.h>

void common_init(int argc, char** argv);
int start_handler(
    std::shared_ptr<apache::thrift::TProcessor> processor,
    std::string const& name);
