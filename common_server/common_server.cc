#include "common_server.h"

#include <gflags/gflags.h>
#include <glog/logging.h>

#include <protocol/TBinaryProtocol.h>
#include <protocol/TCompactProtocol.h>
#include <server/TServer.h>
#include <server/TSimpleServer.h>
#include <server/TThreadPoolServer.h>
#include <transport/TServerSocket.h>
#include <transport/TTransportUtils.h>

#include <concurrency/PosixThreadFactory.h>
#include <concurrency/ThreadManager.h>

DEFINE_string(bind, "localhost", "Host to bind the server to");
DEFINE_int32(port, 9090, "Port to start the server on");
DEFINE_bool(concurrent, false, "Multithreaded server?");
DEFINE_int32(threads, 4, "How many threads to use");
DEFINE_string(protocol, "binary", "Thrift protocol to use");

DEFINE_string(path, "", "Path to logstore database folder");
DEFINE_bool(create_if_missing, false, "Path to logstore database folder");
DEFINE_bool(no_diffing, false, "Disable binary diffing");
DEFINE_bool(no_flush, false, "Disable flushing the WAL after each put");
DEFINE_bool(no_compression, false, "Disable compression");

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;
using namespace apache::thrift::concurrency;

namespace {
TServer* gserver{nullptr};
void signal_handler(int signum) {
  LOG(INFO) << "Stopping...";
  if (signum == SIGINT) {
    gserver->stop();
  }
}

void serve_and_wait(TServer* server) {
  gserver = server;
  signal(SIGINT, signal_handler);
  server->serve();
}

} // namespace

void common_init(int argc, char** argv) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);
  FLAGS_logtostderr = 1;
}

int start_handler(
    std::shared_ptr<TProcessor> processor,
    std::string const& name) {
  LOG(INFO) << "Starting " << name << " on " << FLAGS_bind << ":" << FLAGS_port;

  std::shared_ptr<TServerTransport> serverTransport(
      new TServerSocket(FLAGS_bind, FLAGS_port));
  std::shared_ptr<TTransportFactory> transportFactory(
      new TBufferedTransportFactory());
  std::string prot = FLAGS_protocol;
  std::shared_ptr<TProtocolFactory> protocolFactory;
  if (prot == "binary") {
    protocolFactory = std::make_shared<TBinaryProtocolFactory>();
  } else if (prot == "compact") {
    protocolFactory = std::make_shared<TCompactProtocolFactory>();
  } else {
    LOG(FATAL) << "invalid protocol " << prot;
  }

  if (FLAGS_concurrent) {
    LOG(INFO) << "[Concurrent]";
    CHECK_LE(FLAGS_threads, 20) << "Too many threads";
    CHECK_GE(FLAGS_threads, 1) << "Too few threads";
    auto threadManager = ThreadManager::newSimpleThreadManager(FLAGS_threads);
    auto threadFactory = std::make_shared<PosixThreadFactory>();
    threadFactory->setDetached(false);
    threadManager->threadFactory(threadFactory);
    threadManager->start();
    TThreadPoolServer server(
        processor,
        serverTransport,
        transportFactory,
        protocolFactory,
        threadManager);
    serve_and_wait(&server);
    LOG(INFO) << "after serve_and_wait()";
    threadManager->stop();
    LOG(INFO) << "after stop()";
  } else {
    LOG(INFO) << "[Simple]";
    TSimpleServer server(
        processor, serverTransport, transportFactory, protocolFactory);
    serve_and_wait(&server);
  }

  signal(SIGINT, SIG_DFL);

  LOG(INFO) << "Stopped";

  return 0;
}
