cmake_minimum_required(VERSION 3.2)
project(all_the_tags CXX C)

enable_testing()

set(CMAKE_CXX_STANDARD 14)

if (${CMAKE_BUILD_TYPE} MATCHES "Debug")
    message("Building with ASAN/LSAN")
    if (${CMAKE_SYSTEM_NAME} MATCHES Linux)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address,leak")
    else ()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
    endif ()
endif ()

SET(CMAKE_CXX_FLAGS_DEBUG  "-O0 -g -DDEBUG")

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/")
include(ExternalProject)
include(${CMAKE_SOURCE_DIR}/cmake/FindFolly.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/InstallFolly.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/External_GTest.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/FindGlog.cmake)

find_package(Boost REQUIRED)
find_package(Thrift REQUIRED)
# find_package(GTest REQUIRED)

if (UNIX AND NOT APPLE)
  link_libraries("-Wl,--no-as-needed -ldl")
endif()

message("thrift at: ${THRIFT_INCLUDE_DIR}")

include_directories(common_server)
add_library(common_server STATIC
    common_server/common_server.cc)
add_dependencies(common_server folly)
target_link_libraries(common_server ${GLOG_LIBRARY} ${GFLAGS_LIBRARY})

include_directories(src)
add_library(all_the_tags STATIC
    src/context.cc
    src/tag.cc)
add_dependencies(all_the_tags folly)
target_link_libraries(all_the_tags ${GLOG_LIBRARY} ${GFLAGS_LIBRARY})

message("Folly include dir: ${FOLLY_INCLUDE_DIR}")
include_directories(${FOLLY_INCLUDE_DIR})

include_directories(test)

set(GEN_FILES_PATH "${CMAKE_CURRENT_BINARY_DIR}/gen")
include_directories(${GEN_FILES_PATH}/gen-cpp)
include_directories(${CMAKE_SOURCE_DIR}/common_if)

set(THRIFT_FLAGS --gen cpp -I ${CMAKE_SOURCE_DIR}/common_if -o ${GEN_FILES_PATH})

add_custom_command(
OUTPUT
    ${GEN_FILES_PATH}/gen-cpp/ConcurrentService.cpp
    ${GEN_FILES_PATH}/gen-cpp/ConcurrentService.h
    ${GEN_FILES_PATH}/gen-cpp/concurrent_service_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/concurrent_service_if_types.h
COMMAND mkdir -p ${GEN_FILES_PATH}
COMMAND ${THRIFT_COMPILER} ${THRIFT_FLAGS} ${CMAKE_SOURCE_DIR}/common_if/concurrent_service_if.thrift
DEPENDS
    ${THRIFT_COMPILER} common_if/concurrent_service_if.thrift
WORKING_DIRECTORY
    ${CMAKE_BINARY_DIR}
)

add_library(concurrent_service_handler
    common_if/concurrent_service_handler.cc
    ${GEN_FILES_PATH}/gen-cpp/concurrent_service_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/ConcurrentService.cpp
    )
target_link_libraries(concurrent_service_handler double-conversion)

add_custom_command(
OUTPUT
    ${GEN_FILES_PATH}/gen-cpp/AttServer.cpp
    ${GEN_FILES_PATH}/gen-cpp/AttServer.h
    ${GEN_FILES_PATH}/gen-cpp/att_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/att_if_types.h
COMMAND mkdir -p ${GEN_FILES_PATH}
COMMAND ${THRIFT_COMPILER} ${THRIFT_FLAGS} ${CMAKE_SOURCE_DIR}/server/att_if.thrift
DEPENDS
    ${THRIFT_COMPILER} server/att_if.thrift ${GEN_FILES_PATH}/gen-cpp/concurrent_service_if_types.h
WORKING_DIRECTORY
    ${CMAKE_BINARY_DIR}
)

add_custom_command(
OUTPUT
    ${GEN_FILES_PATH}/gen-cpp/StringSim.cpp
    ${GEN_FILES_PATH}/gen-cpp/StringSim.h
    ${GEN_FILES_PATH}/gen-cpp/stringsim_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/stringsim_if_types.h
COMMAND mkdir -p ${GEN_FILES_PATH}
COMMAND ${THRIFT_COMPILER} ${THRIFT_FLAGS} ${CMAKE_SOURCE_DIR}/stringsim_server/stringsim_if.thrift
DEPENDS
    ${THRIFT_COMPILER} stringsim_server/stringsim_if.thrift ${GEN_FILES_PATH}/gen-cpp/concurrent_service_if_types.h
WORKING_DIRECTORY
    ${CMAKE_BINARY_DIR}
)

include_directories(${THRIFT_INCLUDE_DIR}/thrift)
add_executable(all_the_tags_server
    server/server.cc
    server/server_handler.cc
    ${GEN_FILES_PATH}/gen-cpp/att_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/AttServer.cpp)
target_link_libraries(all_the_tags_server
    all_the_tags gtest_main folly common_server concurrent_service_handler ${THRIFT_LIBRARIES})

add_executable(all_the_tags_client
    server/client.cc
    ${GEN_FILES_PATH}/gen-cpp/att_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/AttServer.cpp
    ${GEN_FILES_PATH}/gen-cpp/concurrent_service_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/ConcurrentService.cpp
)
target_link_libraries(all_the_tags_client
    all_the_tags gtest_main folly ${THRIFT_LIBRARIES})

add_executable(all_the_tags_tests
    test/test.cc
    test/test_query.cc)
add_dependencies(all_the_tags_tests
    googletest)
target_link_libraries(all_the_tags_tests
    all_the_tags gtest_main folly)

add_executable(stringsim_server
    stringsim_server/stringsim_server.cc
    stringsim_server/stringsim_server_handler.cc
    ${GEN_FILES_PATH}/gen-cpp/stringsim_if_types.cpp
    ${GEN_FILES_PATH}/gen-cpp/StringSim.cpp)
target_link_libraries(stringsim_server
    gtest_main folly common_server concurrent_service_handler ${THRIFT_LIBRARIES})
